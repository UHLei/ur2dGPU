name: "s1d_2SA4C_vis"

# ----------- Read data for training -----------------
layer {
  name: "data"
  type: "Input"
  top: "data"
  input_param { shape: { dim: 1 dim: 3 dim: 160 dim: 160 } }
}

# ----------------------------------------------


# -------------------- D-VGG network -------------------------------------------

layer {  bottom: "data"  top: "conv1_1"  name: "conv1_1"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 64    pad: 1    kernel_size: 3      weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer {  bottom: "conv1_1" top: "conv1_1" name: "bn1_1" type: "BatchNorm"}
layer {
  bottom: "conv1_1"
  top: "conv1_1"
  name: "conv1_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}

layer {  bottom: "conv1_1"  top: "conv1_1"  name: "relu1_1"  type: "ReLU"}
# conv1_2
layer {  bottom: "conv1_1"  top: "conv1_2"  name: "conv1_2"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 64    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv1_2' top: 'conv1_2' name: 'bn1_2' type: "BatchNorm"}
layer {
  bottom: "conv1_2"
  top: "conv1_2"
  name: "conv1_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}


layer {  bottom: "conv1_2"  top: "conv1_2"  name: "relu1_2"  type: "ReLU"}

# pool1
layer {
  bottom: "conv1_2"  top: "pool1" name: "pool1"  type: "Pooling"
  pooling_param {    pool: MAX    kernel_size: 2    stride: 2  }
}


# 112 x 112
# conv2_1
layer {  bottom: "pool1"  top: "conv2_1"  name: "conv2_1"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 128    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv2_1' top: 'conv2_1' name: 'bn2_1' type: "BatchNorm"}
layer {
  bottom: "conv2_1"
  top: "conv2_1"
  name: "conv2_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv2_1"  top: "conv2_1"  name: "relu2_1"  type: "ReLU"}
# conv2_2
layer {  bottom: "conv2_1"  top: "conv2_2"  name: "conv2_2"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 128    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer {  bottom: 'conv2_2' top: 'conv2_2' name: 'bn2_2' type: "BatchNorm"}
layer {
  bottom: "conv2_2"
  top: "conv2_2"
  name: "conv2_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv2_2"  top: "conv2_2"  name: "relu2_2"  type: "ReLU"}

# pool2
layer {
  bottom: "conv2_2"  top: "pool2" top: "pool2_mask" name: "pool2"  type: "Pooling"
  pooling_param {    pool: MAX    kernel_size: 2    stride: 2  }
}

# 56 x 56
# conv3_1
layer {  bottom: "pool2"  top: "conv3_1"  name: "conv3_1"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 256    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv3_1' top: 'conv3_1' name: 'bn3_1' type: "BatchNorm"}
layer {
  bottom: "conv3_1"
  top: "conv3_1"
  name: "conv3_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv3_1"  top: "conv3_1"  name: "relu3_1"  type: "ReLU"}
# conv3_2
layer {  bottom: "conv3_1"  top: "conv3_2"  name: "conv3_2"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 256    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv3_2' top: 'conv3_2' name: 'bn3_2' type: "BatchNorm"}
layer {
  bottom: "conv3_2"
  top: "conv3_2"
  name: "conv3_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv3_2"  top: "conv3_2"  name: "relu3_2"  type: "ReLU"}
# conv3_3
layer {  bottom: "conv3_2"  top: "conv3_3"  name: "conv3_3"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 256    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv3_3' top: 'conv3_3' name: 'bn3_3' type: "BatchNorm"}
layer {
  bottom: "conv3_3"
  top: "conv3_3"
  name: "conv3_3_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv3_3"  top: "conv3_3"  name: "relu3_3"  type: "ReLU"}

# pool3
layer {
  bottom: "conv3_3"  top: "pool3" top: "pool3_mask"  name: "pool3"  type: "Pooling"
  pooling_param {    pool: MAX    kernel_size: 2    stride: 2  }
}

# 28 x 28
# conv4_1
layer {  bottom: "pool3"  top: "conv4_1"  name: "conv4_1"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 512    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv4_1' top: 'conv4_1' name: 'bn4_1' type: "BatchNorm" }
layer {
  bottom: "conv4_1"
  top: "conv4_1"
  name: "conv4_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv4_1"  top: "conv4_1"  name: "relu4_1"  type: "ReLU"}
# conv4_2
layer {  bottom: "conv4_1"  top: "conv4_2"  name: "conv4_2"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 512    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv4_2' top: 'conv4_2' name: 'bn4_2' type: "BatchNorm"}
layer {
  bottom: "conv4_2"
  top: "conv4_2"
  name: "conv4_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv4_2"  top: "conv4_2"  name: "relu4_2"  type: "ReLU"}
# conv4_3
layer {  bottom: "conv4_2"  top: "conv4_3"  name: "conv4_3"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 512    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv4_3' top: 'conv4_3' name: 'bn4_3' type: "BatchNorm"}
layer {
  bottom: "conv4_3"
  top: "conv4_3"
  name: "conv4_3_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv4_3"  top: "conv4_3"  name: "relu4_3"  type: "ReLU"}

# pool4
layer {
  bottom: "conv4_3"  top: "pool4" top: "pool4_mask"  name: "pool4"  type: "Pooling"
  pooling_param {    pool: MAX    kernel_size: 2    stride: 2  }
}

# 14 x 14
# conv5_1
layer {  bottom: "pool4"  top: "conv5_1"  name: "conv5_1"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 512    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv5_1' top: 'conv5_1' name: 'bn5_1' type: "BatchNorm"}
layer {
  bottom: "conv5_1"
  top: "conv5_1"
  name: "conv5_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv5_1"  top: "conv5_1"  name: "relu5_1"  type: "ReLU"}
# conv5_2
layer {  bottom: "conv5_1"  top: "conv5_2"  name: "conv5_2"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 512    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv5_2' top: 'conv5_2' name: 'bn5_2' type: "BatchNorm" }
layer {
  bottom: "conv5_2"
  top: "conv5_2"
  name: "conv5_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv5_2"  top: "conv5_2"  name: "relu5_2"  type: "ReLU"}
# conv5_3
layer {  bottom: "conv5_2"  top: "conv5_3"  name: "conv5_3"  type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param {    num_output: 512    pad: 1    kernel_size: 3  
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'conv5_3' top: 'conv5_3' name: 'bn5_3' type: "BatchNorm"}
layer {
  bottom: "conv5_3"
  top: "conv5_3"
  name: "conv5_3_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "conv5_3"  top: "conv5_3"  name: "relu5_3"  type: "ReLU"}

# pool5
layer {
  bottom: "conv5_3"  top: "pool5" top: "pool5_mask"  name: "pool5"  type: "Pooling"
  pooling_param {    pool: MAX    kernel_size: 2    stride: 2  }
}


# 5 x 5
# fc6
layer { bottom: 'pool5' top: 'fc6' name: 'fc6' type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { kernel_size: 5 num_output: 4096 
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'fc6' top: 'fc6' name: 'bnfc6' type: "BatchNorm" }
layer {
  bottom: "fc6"
  top: "fc6"
  name: "fc6_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer {  bottom: "fc6"  top: "fc6"  name: "relu6"  type: "ReLU"}

layer{
  name: "silence_label_2"
  type: "Silence"
  bottom: "pool3_mask"
  bottom: "pool2_mask"
  bottom: "pool5_mask"
  bottom: "pool4_mask"
}


layer { bottom: 'fc6' top: 'fc7' name: 'fc7' type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { kernel_size: 1 num_output: 4096
  weight_filler { type: "gaussian"      std: 0.01}  bias_term: false}}
layer { bottom: 'fc7' top: 'fc7' name: 'bnfc7' type: "BatchNorm"}
layer {
  bottom: "fc7"
  top: "fc7"
  name: "bnfc7_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}

layer {  bottom: "fc7"  top: "fc7"  name: "relu7"  type: "ReLU"}
# 5x5 
# fc6-deconv
layer { bottom: 'fc7' top: 'fc6-deconv' name: 'fc6-deconv' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 512 kernel_size: 5
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'fc6-deconv' top: 'fc6-deconv' name: 'fc6-deconv-bn' type: "BatchNorm" }
 layer {
  bottom: "fc6-deconv"
  top: "fc6-deconv"
  name: "fc6-deconv-bn_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}


layer { bottom: 'fc6-deconv' top: 'fc6-deconv' name: 'fc6-deconv-relu' type: "ReLU" }


# 10 x 10
# unpool5
layer { type: "Unpooling"  bottom: "fc6-deconv"  bottom: "pool5_mask"  top: "unpool5"  name: "unpool5"
  unpooling_param {   unpool: MAX   kernel_size: 2    stride: 2   unpool_size: 10 }
}



# deconv5_1
layer { bottom: 'unpool5' top: 'deconv5_1' name: 'deconv5_1' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 512 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv5_1' top: 'deconv5_1' name: 'debn5_1' type: "BatchNorm" }
layer {
  bottom: "deconv5_1"
  top: "deconv5_1"
  name: "debn5_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'deconv5_1' top: 'deconv5_1' name: 'derelu5_1' type: "ReLU" }
# deconv5_2
layer { bottom: 'deconv5_1' top: 'deconv5_2' name: 'deconv5_2' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 512 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv5_2' top: 'deconv5_2' name: 'debn5_2' type: "BatchNorm"}
layer {
  bottom: "deconv5_2"
  top: "deconv5_2"
  name: "debn5_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}

layer { bottom: 'deconv5_2' top: 'deconv5_2' name: 'derelu5_2' type: "ReLU" }
# deconv5_3
layer { bottom: 'deconv5_2' top: 'deconv5_3' name: 'deconv5_3' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 512 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv5_3' top: 'deconv5_3' name: 'debn5_3' type: "BatchNorm" }
layer {
  bottom: "deconv5_3"
  top: "deconv5_3"
  name: "debn5_3_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}

layer { bottom: 'deconv5_3' top: 'deconv5_3' name: 'derelu5_3' type: "ReLU" }

# unpool4
layer { type: "Unpooling"  bottom: "deconv5_3"  bottom: "pool4_mask"  top: "unpool4"  name: "unpool4"
  unpooling_param {   unpool: MAX   kernel_size: 2    stride: 2   unpool_size: 20 }
}

# 20 x 20
# deconv4_1
layer { bottom: 'unpool4' top: 'deconv4_1' name: 'deconv4_1' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 512 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv4_1' top: 'deconv4_1' name: 'debn4_1' type: "BatchNorm" }
layer {
  bottom: "deconv4_1"
  top: "deconv4_1"
  name: "deconv4_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}


layer { bottom: 'deconv4_1' top: 'deconv4_1' name: 'derelu4_1' type: "ReLU" }
layer{
  bottom: "deconv4_1"
  bottom: "conv4_1"
  top: "sum_conv4_1"
  type: "Eltwise"
  name: "sum_conv4_1"
}






# deconv 4_2
layer { bottom: 'sum_conv4_1' top: 'deconv4_2' name: 'deconv4_2' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 512 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv4_2' top: 'deconv4_2' name: 'debn4_2' type: "BatchNorm" }
layer {
  bottom: "deconv4_2"
  top: "deconv4_2"
  name: "deconv4_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'deconv4_2' top: 'deconv4_2' name: 'derelu4_2' type: "ReLU" }
# deconv 4_3
layer { bottom: 'deconv4_2' top: 'deconv4_3' name: 'deconv4_3' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 256 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv4_3' top: 'deconv4_3' name: 'debn4_3' type: "BatchNorm" }
layer {
  bottom: "deconv4_3"
  top: "deconv4_3"
  name: "deconv4_3_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'deconv4_3' top: 'deconv4_3' name: 'derelu4_3' type: "ReLU" }



layer { name: 'seg-scorex4' type: "Convolution" bottom: 'deconv4_3' top: 'seg-scorex4'
  param {lr_mult: 1 decay_mult:1} param{lr_mult:2 decay_mult:0 }
  convolution_param { num_output: 58 kernel_size: 1
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_filler {      type: "constant"      value: 0    }} }






layer {
  name: "Feature_x4_Upsampling" type: "Deconvolution"
  bottom: "seg-scorex4" top: "Feature_x4_Upsampling"
  convolution_param {
    kernel_size: 4 stride: 2 pad: 1
    num_output: 58  group: 58
    weight_filler: { type: "bilinear" } 
    bias_term: false }
  param { lr_mult: 0 decay_mult: 0 }
}
layer{
 name: "Concat3"
 bottom: "Feature_x4_Upsampling"
 bottom: "conv3_1"
 top: "Concat3"
 type: "Concat"
}
layer { bottom: 'Concat3' top: 'conv3_1_pathway_cov1' name: 'conv3_1_pathway_cov1' type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 256 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'conv3_1_pathway_cov1' top: 'conv3_1_pathway_cov1' name: 'conv3_1_pathway_cov1_bn' type: "BatchNorm" }
layer {
  bottom: "conv3_1_pathway_cov1"
  top: "conv3_1_pathway_cov1"
  name: "dconv3_1_pathway_cov1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'conv3_1_pathway_cov1' top: 'conv3_1_pathway_cov1' name: 'conv3_1_pathway_cov1_relu' type: "ReLU" }
layer { bottom: 'conv3_1_pathway_cov1' top: 'conv3_1_pathway_cov2' name: 'conv3_1_pathway_cov2' type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 256 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'conv3_1_pathway_cov2' top: 'conv3_1_pathway_cov2' name: 'conv3_1_pathway_cov2_bn' type: "BatchNorm" }
layer {
  bottom: "conv3_1_pathway_cov2"
  top: "conv3_1_pathway_cov2"
  name: "conv3_1_pathway_cov2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'conv3_1_pathway_cov2' top: 'conv3_1_pathway_cov2' name: 'conv3_1_pathway_cov2_relu' type: "ReLU" }
layer { name: 'conv3_1_pathway_transform' type: "Convolution" bottom: 'conv3_1_pathway_cov2' top: 'conv3_1_pathway_transform'
  param {lr_mult: 1 decay_mult:1} param{lr_mult:2 decay_mult:0 }
  convolution_param { num_output: 58 kernel_size: 1
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_filler {      type: "constant"      value: 0    }}}

# Softmax for this channel

layer{
   bottom: "conv3_1_pathway_transform"
   bottom: "Feature_x4_Upsampling"
   top: "x2_combining1"
   type: "Eltwise"
   name: "x2_combining1"
   eltwise_param { operation: PROD }
}
layer{
   bottom: "x2_combining1"
   bottom: "Feature_x4_Upsampling"
   top: "x2_combining2"
   type: "Eltwise"
   name: "x2_combining2"
}



# unpool3
layer { type: "Unpooling"  bottom: "deconv4_3"  bottom: "pool3_mask"  top: "unpool3"  name: "unpool3"
  unpooling_param {   unpool: MAX   kernel_size: 2    stride: 2   unpool_size: 40 }
}

# 40 x 40
# deconv3_1
layer { bottom: 'unpool3' top: 'deconv3_1' name: 'deconv3_1' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output:256  pad:1 kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv3_1' top: 'deconv3_1' name: 'debn3_1' type: "BatchNorm" }
layer {
  bottom: "deconv3_1"
  top: "deconv3_1"
  name: "deconv3_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}

layer { bottom: 'deconv3_1' top: 'deconv3_1' name: 'derelu3_1' type: "ReLU" }
layer{
  bottom: "deconv3_1"
  bottom: "x2_combining2"
  top: "Concat_deconv3_1"
  type: "Concat"
  name: "Concat_deconv3_1"
}

# deconv3_2
layer { bottom: 'Concat_deconv3_1' top: 'deconv3_2' name: 'deconv3_2' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output:256  pad:1 kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv3_2' top: 'deconv3_2' name: 'debn3_2' type: "BatchNorm" }
layer {
  bottom: "deconv3_2"
  top: "deconv3_2"
  name: "deconv3_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'deconv3_2' top: 'deconv3_2' name: 'derelu3_2' type: "ReLU" }

# deconv3_3
layer { bottom: 'deconv3_2' top: 'deconv3_3' name: 'deconv3_3' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output:128  pad:1 kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv3_3' top: 'deconv3_3' name: 'debn3_3' type: "BatchNorm" }
layer {
  bottom: "deconv3_3"
  top: "deconv3_3"
  name: "deconv3_3_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'deconv3_3' top: 'deconv3_3' name: 'derelu3_3' type: "ReLU" }


layer { name: 'seg-scorex2' type: "Convolution" bottom: 'deconv3_3' top: 'seg-scorex2'
  param {lr_mult: 1 decay_mult:1} param{lr_mult:2 decay_mult:0 }
  convolution_param { num_output: 58 kernel_size: 1
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_filler {      type: "constant"      value: 0    }} }



# -----------------Information pathway------------------


layer{
   bottom: 'seg-scorex2'
   bottom: "x2_combining2"
   top: "x2_combining3"
   type: "Eltwise"
   name: "x2_combining3"
   eltwise_param { operation: PROD }
}


layer{
   bottom: "x2_combining2"
   bottom: "x2_combining3"
   top: "intermediate_stage_output"
   type: "Eltwise"
   name: "intermediate_stage_output"
}



# Bilinear feature map upsampling

layer {
  name: "Feature_x2_Upsampling" type: "Deconvolution"
  bottom: "intermediate_stage_output" top: "Feature_x2_Upsampling"
  convolution_param {
    kernel_size: 4 stride: 2 pad: 1
    num_output: 58  group: 58
    weight_filler: { type: "bilinear" } 
    bias_term: false }
  param { lr_mult: 0 decay_mult: 0 }
}



# Process L3 layers based to courage False Alarms but remain high recall curve.

layer{
 name: "Concat2"
 bottom: "Feature_x2_Upsampling"
 bottom: "conv2_1"
 top: "Concat2"
 type: "Concat"
}

layer { bottom: 'Concat2' top: 'conv2_1_pathway_cov1' name: 'conv2_1_pathway_cov1' type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 256 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'conv2_1_pathway_cov1' top: 'conv2_1_pathway_cov1' name: 'conv2_1_pathway_cov1_bn' type: "BatchNorm" }
layer {
  bottom: "conv2_1_pathway_cov1"
  top: "conv2_1_pathway_cov1"
  name: "dconv2_1_pathway_cov1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'conv2_1_pathway_cov1' top: 'conv2_1_pathway_cov1' name: 'conv2_1_pathway_cov1_relu' type: "ReLU" }


layer { bottom: 'conv2_1_pathway_cov1' top: 'conv2_1_pathway_cov2' name: 'conv2_1_pathway_cov2' type: "Convolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output: 256 pad: 1  kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'conv2_1_pathway_cov2' top: 'conv2_1_pathway_cov2' name: 'conv2_1_pathway_cov2_bn' type: "BatchNorm" }
layer {
  bottom: "conv2_1_pathway_cov2"
  top: "conv2_1_pathway_cov2"
  name: "conv2_1_pathway_cov2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'conv2_1_pathway_cov2' top: 'conv2_1_pathway_cov2' name: 'conv2_1_pathway_cov2_relu' type: "ReLU" }


layer { name: 'conv2_1_pathway_transform' type: "Convolution" bottom: 'conv2_1_pathway_cov2' top: 'conv2_1_pathway_transform'
  param {lr_mult: 1 decay_mult:1} param{lr_mult:2 decay_mult:0 }
  convolution_param { num_output: 58 kernel_size: 1
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_filler {      type: "constant"      value: 0    }}}



# Softmax for this channel


layer{
   bottom: "conv2_1_pathway_transform"
   bottom: "Feature_x2_Upsampling"
   top: "x1_combining1"
   type: "Eltwise"
   name: "x1_combining1"
   eltwise_param { operation: PROD }
}


layer{
   bottom: "x1_combining1"
   bottom: "Feature_x2_Upsampling"
   top: "x1_combining2"
   type: "Eltwise"
   name: "x1_combining2"
}



# unpool2
layer { type: "Unpooling"  bottom: "deconv3_3"  bottom: "pool2_mask"  top: "unpool2"  name: "unpool2"
  unpooling_param {   unpool: MAX   kernel_size: 2    stride: 2   unpool_size: 80 }
}

layer{
  bottom: "unpool2"
  bottom: "x1_combining2"
  top: "Concat_unpool2"
  type: "Concat"
  name: "Concat_unpool2"
}
# 80 x 80
# deconv2_1
layer { bottom: 'Concat_unpool2' top: 'deconv2_1' name: 'deconv2_1' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output:128  pad:1 kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv2_1' top: 'deconv2_1' name: 'debn2_1' type: "BatchNorm" }
layer {
  bottom: "deconv2_1"
  top: "deconv2_1"
  name: "deconv2_1_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'deconv2_1' top: 'deconv2_1' name: 'derelu2_1' type: "ReLU" }
# deconv2_2
layer { bottom: 'deconv2_1' top: 'deconv2_2' name: 'deconv2_2' type: "Deconvolution"
  param {lr_mult: 1 decay_mult:1}  
  convolution_param { num_output:128  pad:1 kernel_size: 3
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_term: false} }
layer { bottom: 'deconv2_2' top: 'deconv2_2' name: 'debn2_2' type: "BatchNorm" }
layer {
  bottom: "deconv2_2"
  top: "deconv2_2"
  name: "deconv2_2_scale"
  type: "Scale"
  scale_param {
    bias_term: true
  }
}
layer { bottom: 'deconv2_2' top: 'deconv2_2' name: 'derelu2_2' type: "ReLU" }

# seg-score
layer { name: 'seg-score' type: "Convolution" bottom: 'deconv2_2' top: 'seg-score'
  param {lr_mult: 1 decay_mult:1} param{lr_mult:2 decay_mult:0 }
  convolution_param { num_output: 58 kernel_size: 1
    weight_filler {      type: "gaussian"      std: 0.01    }
    bias_filler {      type: "constant"      value: 0    }} }



layer{
   bottom: "x1_combining2"
   bottom: "seg-score"
   top: "x1_combining1_combined"
   type: "Eltwise"
   name: "x1_combining1_combined"
   eltwise_param { operation: PROD }
}

layer{
   bottom: "x1_combining1_combined"
   bottom: "x1_combining2"
   top: "final_output"
   type: "Eltwise"
   name: "final_output"
}



layer {
  name: "lossx4"
  type: "Softmax"
  bottom: "seg-scorex4"

  top: "lossx4"

}

layer {
  name: "Loss_x2_combining2N"
  type: "Softmax"
  bottom: "x2_combining2"

  top: "Loss_x2_combining2N"

}

layer {
  name: "Loss_IntermediateN"
  type: "Softmax"
  bottom: "intermediate_stage_output"

  top: "Loss_IntermediateN"

}

layer {
  name: "Loss_J3_intermediateN"
  type: "Softmax"
  bottom: "x1_combining2"

  top: "Loss_J3_intermediateN"

}

layer {
  name: "Loss_J3N"
  type: "Softmax"
  bottom: "final_output"

  top: "Loss_J3N"

}


layer {
  name: "slice_label"
  type: "Slice"
  bottom: "Loss_J3N"
  top: "sAA_11"
  top: "sAA_12"
  top: "sAA_13"
  top: "sAA_21"
  top: "sAA_22"
  top: "sAA_23"
  top: "sAA_31"
  top: "sAA_32"
  top: "sAA_33"
  top: "sAA_41"
  top: "sAA_42"
  top: "sAA_43"
  top: "sAA_51"
  top: "sAA_52"
  top: "sAA_53"
  top: "sAA_61"
  top: "sAA_62"
  top: "sAA_63"
  top: "sAA_71"
  top: "sAA_72"
  top: "sAA_73"
  top: "sAA_81"
  top: "sAA_82"
  top: "sAA_83"
  top: "sAA_91"
  top: "sAA_92"
  top: "sAA_93"
  top: "sAA_101"
  top: "sAA_102"
  top: "sAA_103"
  top: "sAA_111"
  top: "sAA_112"
  top: "sAA_113"
  top: "sAA_121"
  top: "sAA_122"
  top: "sAA_123"
  top: "sAA_131"
  top: "sAA_132"
  top: "sAA_133"
  top: "sAA_141"
  top: "sAA_142"
  top: "sAA_143"
  top: "sAA_151"
  top: "sAA_152"
  top: "sAA_153"
  top: "sAA_161"
  top: "sAA_162"
  top: "sAA_163"
  top: "sAA_171"
  top: "sAA_172"
  top: "sAA_173"
  top: "sAA_181"
  top: "sAA_182"
  top: "sAA_183"
  top: "sAA_191"
  top: "sAA_192"
  top: "sAA_193"

  top: "background"
  slice_param {
    axis: 1
    slice_point: 1
    slice_point: 2
    slice_point: 3
    slice_point: 4
    slice_point: 5
    slice_point: 6
    slice_point: 7
    slice_point: 8
    slice_point: 9
    slice_point: 10
    slice_point: 11
    slice_point: 12
    slice_point: 13
    slice_point: 14
    slice_point: 15
    slice_point: 16
    slice_point: 17
    slice_point: 18
    slice_point: 19
    slice_point: 20
    slice_point: 21
    slice_point: 22
    slice_point: 23
    slice_point: 24
    slice_point: 25
    slice_point: 26
    slice_point: 27
    slice_point: 28
    slice_point: 29
    slice_point: 30
    slice_point: 31
    slice_point: 32
    slice_point: 33
    slice_point: 34
    slice_point: 35
    slice_point: 36
    slice_point: 37
    slice_point: 38
    slice_point: 39
    slice_point: 40
    slice_point: 41
    slice_point: 42
    slice_point: 43
    slice_point: 44
    slice_point: 45
    slice_point: 46
    slice_point: 47
    slice_point: 48
    slice_point: 49
    slice_point: 50
    slice_point: 51
    slice_point: 52
    slice_point: 53
    slice_point: 54
    slice_point: 55
    slice_point: 56
    slice_point: 57

  }
}
layer{
   bottom:"background"
bottom: "Loss_x2_combining2N"
bottom: "lossx4"
bottom: "Loss_IntermediateN"
bottom: "Loss_J3_intermediateN"
   type: "Silence"
   name: "background_Silence"
}

layer{
  name: "Concat_ldmk_score_1"
  type: "Eltwise"
  bottom: "sAA_11"
  bottom: "sAA_12"
  bottom: "sAA_13"
  top: "sAA_1_combine"
}



layer{
  name: "Concat_ldmk_score_2"
  type: "Eltwise"
  bottom: "sAA_21"
  bottom: "sAA_22"
  bottom: "sAA_23"
  top: "sAA_2_combine"
}



layer{
  name: "Concat_ldmk_score_3"
  type: "Eltwise"
  bottom: "sAA_31"
  bottom: "sAA_32"
  bottom: "sAA_33"
  top: "sAA_3_combine"
}


layer{
  name: "Concat_ldmk_score_4"
  type: "Eltwise"
  bottom: "sAA_41"
  bottom: "sAA_42"
  bottom: "sAA_43"
  top: "sAA_4_combine"
}


layer{
  name: "Concat_ldmk_score_5"
  type: "Eltwise"
  bottom: "sAA_51"
  bottom: "sAA_52"
  bottom: "sAA_53"
  top: "sAA_5_combine"
}



layer{
  name: "Concat_ldmk_score_6"
  type: "Eltwise"
  bottom: "sAA_61"
  bottom: "sAA_62"
  bottom: "sAA_63"
  top: "sAA_6_combine"
}


layer{
  name: "Concat_ldmk_score_7"
  type: "Eltwise"
  bottom: "sAA_71"
  bottom: "sAA_72"
  bottom: "sAA_73"
  top: "sAA_7_combine"
}



layer{
  name: "Concat_ldmk_score_8"
  type: "Eltwise"
  bottom: "sAA_81"
  bottom: "sAA_82"
  bottom: "sAA_83"
  top: "sAA_8_combine"
}



layer{
  name: "Concat_ldmk_score_9"
  type: "Eltwise"
  bottom: "sAA_91"
  bottom: "sAA_92"
  bottom: "sAA_93"
  top: "sAA_9_combine"
}



layer{
  name: "Concat_ldmk_score_10"
  type: "Eltwise"
  bottom: "sAA_101"
  bottom: "sAA_102"
  bottom: "sAA_103"
  top: "sAA_10_combine"
}



layer{
  name: "Concat_ldmk_score_11"
  type: "Eltwise"
  bottom: "sAA_111"
  bottom: "sAA_112"
  bottom: "sAA_113"
  top: "sAA_11_combine"
}



layer{
  name: "Concat_ldmk_score_12"
  type: "Eltwise"
  bottom: "sAA_121"
  bottom: "sAA_122"
  bottom: "sAA_123"
  top: "sAA_12_combine"
}



layer{
  name: "Concat_ldmk_score_13"
  type: "Eltwise"
  bottom: "sAA_131"
  bottom: "sAA_132"
  bottom: "sAA_133"
  top: "sAA_13_combine"
}



layer{
  name: "Concat_ldmk_score_14"
  type: "Eltwise"
  bottom: "sAA_141"
  bottom: "sAA_142"
  bottom: "sAA_143"
  top: "sAA_14_combine"
}


layer{
  name: "Concat_ldmk_score_15"
  type: "Eltwise"
  bottom: "sAA_151"
  bottom: "sAA_152"
  bottom: "sAA_153"
  top: "sAA_15_combine"
}



layer{
  name: "Concat_ldmk_score_16"
  type: "Eltwise"
  bottom: "sAA_161"
  bottom: "sAA_162"
  bottom: "sAA_163"
  top: "sAA_16_combine"
}



layer{
  name: "Concat_ldmk_score_17"
  type: "Eltwise"
  bottom: "sAA_171"
  bottom: "sAA_172"
  bottom: "sAA_173"
  top: "sAA_17_combine"
}



layer{
  name: "Concat_ldmk_score_18"
  type: "Eltwise"
  bottom: "sAA_181"
  bottom: "sAA_182"
  bottom: "sAA_183"
  top: "sAA_18_combine"
}



layer{
  name: "Concat_ldmk_score_19"
  type: "Eltwise"
  bottom: "sAA_191"
  bottom: "sAA_192"
  bottom: "sAA_193"
  top: "sAA_19_combine"
}





layer{
  type: "Concat"

bottom: "sAA_1_combine"
bottom: "sAA_2_combine"
bottom: "sAA_3_combine"
bottom: "sAA_4_combine"
bottom: "sAA_5_combine"
bottom: "sAA_6_combine"
bottom: "sAA_7_combine"
bottom: "sAA_8_combine"
bottom: "sAA_9_combine"
bottom: "sAA_10_combine"
bottom: "sAA_11_combine"
bottom: "sAA_12_combine"
bottom: "sAA_13_combine"
bottom: "sAA_14_combine"
bottom: "sAA_15_combine"
bottom: "sAA_16_combine"
bottom: "sAA_17_combine"
bottom: "sAA_18_combine"
bottom: "sAA_19_combine"

  top: "Predict_x2D"
  name: "Predict_x2D"
}

